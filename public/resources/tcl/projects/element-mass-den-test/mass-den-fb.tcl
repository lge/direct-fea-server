wipe;
model basic -ndm 2 -ndf 3
geomTransf Linear 1;
geomTransf PDelta 2;
geomTransf Corotational 3;
node 1 0.0 0.0
#node 2 0.0 40.0 -mass 1000 1000 1000;
#node 3 40.0 40.0 -mass 1000 1000 1000;
node 2 0.0 40.0;
node 3 40.0 40.0;
node 4 40.0 0.0
fix 1 1 1 1;
fix 4 1 1 1;

set nz 100;
uniaxialMaterial Elastic 1 29000;

section Fiber 1 {
	patch quad 1 $nz $nz -5.0 -5.0 5.0 -5.0 5.0 5.0 -5.0 5.0;
}
set nintpts 10

element nonlinearBeamColumn 1 1 2 $nintpts 1 3;
#element nonlinearBeamColumn 2 2 3 $nintpts 1 3;
element nonlinearBeamColumn 2 2 3 $nintpts 1 3 -mass 50.0;
element nonlinearBeamColumn 3 3 4 $nintpts 1 3;

puts [eigen 2]
