##########################################################################
# SE207 - Winter 2011 - OpenSees Tutorial
# by Andre R Barbosa
# 2011/02/07
########################################################################## 
#
# Perform time-history response analysis
#
##########################################################################
# define mass
#mass $node $massdirX $massdirY $massrotZ

set LumpedMass [expr $PCol/$g];
mass 3 $LumpedMass $LumpedMass 0.0; 
mass 4 $LumpedMass $LumpedMass 0.0 
mass 5 $LumpedMass $LumpedMass 0.0 
mass 6 $LumpedMass $LumpedMass 0.0 

# define DAMPING
# apply Rayleigh DAMPING from $xDamp
# D=$alphaM*M + $betaKcurr*Kcurrent + $betaKcomm*KlastCommit + $beatKinit*$Kinitial
#set xDamp 0.02;				# 2% damping ratio
#set lambda [eigen 1]; 			# eigenvalue mode 1
#set omega [expr pow($lambda,0.5)];
#set alphaM 0.;				# M-prop. damping; D = alphaM*M
set alphaM 16.95064621
#set 0.00001904643724
set betaKcurr 0.
set betaKcomm 0.00001904643724;         			# K-proportional damping;      +beatKcurr*KCurrent
#set betaKcomm [expr 2.*$xDamp/($omega)];   	# K-prop. damping parameter;   +betaKcomm*KlastCommitt
set betaKinit 0.;         			# initial-stiffness proportional damping      +beatKinit*Kini
rayleigh $alphaM $betaKcurr $betaKinit $betaKcomm; 				# RAYLEIGH damping


set dt 0.02
set input_motion gmotion.txt
timeSeries Path 1 -dt $dt -filePath $input_motion -factor $g
pattern UniformExcitation  10 1 -accel  1;		# create Unifform excitation

constraints Transformation;
numberer Plain
system BandGeneral;	
set  TolDynamic 1.e-8;                        # Convergence Test: tolerance
set  maxNumIterDynamic 10;                # Convergence Test: maximum number of iterations that will be performed before "failure to converge" is returned
set  printFlagDynamic 0;                # Convergence Test: flag used to print information on convergence (optional)        # 1: print information on each step; 
set  testTypeDynamic EnergyIncr;	# Convergence-test type
test $testTypeDynamic $TolDynamic $maxNumIterDynamic $printFlagDynamic;
algorithm Newton;        
set NewmarkGamma 0.5;	# Newmark-integrator gamma parameter (also HHT)
set NewmarkBeta 0.25;	# Newmark-integrator beta parameter
set integratorTypeDynamic Newmark;
integrator $integratorTypeDynamic $NewmarkGamma $NewmarkBeta
analysis Transient
set Nsteps 2500
set ok [analyze $Nsteps $dt];			# actually perform analysis; returns ok=0 if analysis was successful

puts "Ground Motion Done. End Time: [getTime]"