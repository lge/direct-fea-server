

puts " --------------------------------- 2D Model ---------------"
puts " -------------------- "
puts " a. Uniaxial Section"
source Ex9a.build.UniaxialSection2D.tcl
source Ex9.analyze.MomentCurvature2D.tcl

puts " -------------------- "
puts " b. W Section"
source Ex9b.build.WSection2D.tcl
source Ex9.analyze.MomentCurvature2D.tcl


puts " -------------------- "
puts " c. RC Section: Rectangular, Unconfined, Symmetric"
source Ex9c.build.RCSection.RectUnconfinedSymm2D.tcl
source Ex9.analyze.MomentCurvature2D.tcl

puts " -------------------- "
puts " d. RC Section: Rectangular, Confined, Symmetric"
source Ex9d.build.RCSection.RectConfinedSymm2D.tcl
source Ex9.analyze.MomentCurvature2D.tcl

puts " -------------------- "
puts " e. RC Section: Rectangular"
source Ex9e.build.RCSection.Rect2D.tcl
source Ex9.analyze.MomentCurvature2D.tcl

puts " -------------------- "
puts " f. RC Section: Circular"
source Ex9f.build.RCSection.Circ2D.tcl
source Ex9.analyze.MomentCurvature2D.tcl

puts " -------------------- "



puts " --------------------------------- 3D Model ---------------"
puts " -------------------- "
puts " a. Uniaxial Section"
source Ex9a.build.UniaxialSection3D.tcl
source Ex9.analyze.MomentCurvature3D.tcl

puts " -------------------- "
puts " b. W Section"
source Ex9b.build.WSection3D.tcl
source Ex9.analyze.MomentCurvature3D.tcl


puts " -------------------- "
puts " c. RC Section: Rectangular, Unconfined, Symmetric"
source Ex9c.build.RCSection.RectUnconfinedSymm3D.tcl
source Ex9.analyze.MomentCurvature3D.tcl

puts " -------------------- "
puts " d. RC Section: Rectangular, Confined, Symmetric"
source Ex9d.build.RCSection.RectConfinedSymm3D.tcl
source Ex9.analyze.MomentCurvature3D.tcl

puts " -------------------- "
puts " e. RC Section: Rectangular"
source Ex9e.build.RCSection.Rect3D.tcl
source Ex9.analyze.MomentCurvature3D.tcl

puts " -------------------- "
puts " f. RC Section: Circular"
source Ex9f.build.RCSection.Circ3D.tcl
source Ex9.analyze.MomentCurvature3D.tcl

puts " -------------------- "
