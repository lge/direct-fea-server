#PW File TYPE B						
###rigidLink $type $masterNodeTag $slaveNodeTag						
rigidLink	beam	2	101			
rigidLink	beam	5	102			
rigidLink	beam	5	201			
rigidLink	beam	8	202			
rigidLink	beam	8	301			
rigidLink	beam	11	302			
						
element	zeroLength	1001	101	102	-mat $matID -dir 1 ;	
element	zeroLength	1002	201	202	-mat $matID -dir 1 ;	
element	zeroLength	1003	301	302	-mat $matID -dir 1 ;	
equalDOF	101	102	2	3	;	
equalDOF	201	202	2	3	;	
equalDOF	301	302	2	3	;	
