# --------------------------------------------------------------------------------------------------
# static analysis parameters
# I am setting all these variables as global variables (using variable rather than set command)
# so that these variables can be uploaded by a procedure
#                                 Silvia Mazzoni & Frank McKenna, 2006
# Modified By Yong Li, foxchameleon@gmail.com, yol024@ucsd.edu
# Modified Date: Nov. 28,2011, Monday.
# Updated Date: Nov. 28,2011, Monday.
# _______________________________________________________________________________________________________
# _______________________________________________________________________________________________________

# CONSTRAINTS handler -- Determines how the constraint equations are enforced in the analysis (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/617.htm)
#          Plain Constraints -- Removes constrained degrees of freedom from the system of equations (only for homogeneous equations)
#          Lagrange Multipliers -- Uses the method of Lagrange multipliers to enforce constraints 
#          Penalty Method -- Uses penalty numbers to enforce constraints --good for static analysis with non-homogeneous eqns (rigidDiaphragm)
#          Transformation Method -- Performs a condensation of constrained degrees of freedom 
variable constraintsTypeStatic Transformation;		# default;
# constraints $constraintsTypeStatic;
	if {  [info exists RigidDiaphragm] == 1} {
		if {$RigidDiaphragm=="ON"} {
			variable constraintsTypeStatic Lagrange;	#     for large model, try Transformation
			variable alphaS 1e20; # factor on single-point constraints 
			variable alphaM 1e20; # factor on multi-point constraints 
			constraints $constraintsTypeStatic $alphaS $alphaM; 
		};	# if rigid diaphragm is on
	};	# if rigid diaphragm exists

	# if {  [info exists rigidType] == 1} {
		# if {$rigidType=="infinityStiff"} {
			# variable constraintsTypeStatic Penalty;
			# variable alphaS 1e20; #penalty factor on single-point constraints 
			# variable alphaM 1e20; #penalty factor on multi-point constraints 
			# constraints $constraintsTypeStatic $alphaS $alphaM; 
		# };
	# };
puts "YONG:Constraints Type for Static Analysis is: $constraintsTypeStatic";
# _______________________________________________________________________________________________________
# _______________________________________________________________________________________________________

# DOF NUMBERER (number the degrees of freedom in the domain): (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/366.htm)
# determines the mapping between equation numbers and degrees-of-freedom
#          Plain -- Uses the numbering provided by the user 
#          RCM -- Renumbers the DOF to minimize the matrix band-width using the Reverse Cuthill-McKee algorithm 
#          AMD -- Uses the approximate minimum degree scheme to order the matrix equations 
variable numbererTypeStatic Plain; #default
numberer $numbererTypeStatic;
puts "YONG:Numbering Type for Static Analysis is: $numbererTypeStatic";
# _______________________________________________________________________________________________________
# _______________________________________________________________________________________________________

# SYSTEM (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/371.htm)
#   Linear Equation Solvers (how to store and solve the system of equations in the analysis)
#   -- provide the solution of the linear system of equations Ku = P. Each solver is tailored to a specific matrix topology. 
#          ProfileSPD -- Direct profile solver for symmetric positive definite matrices 
#          BandGeneral -- Direct solver for banded unsymmetric matrices 
#          BandSPD -- Direct solver for banded symmetric positive definite matrices 
#          SparseGeneral (Disappear in wiki Nov.28,2011)-- Direct solver for unsymmetric sparse matrices 
#          SparseSPD/SparseSYM-- Direct solver for symmetric sparse matrices 
#          UmfPack -- Direct UmfPack solver for unsymmetric matrices 
#          SparseGEN -- Used for sparse matrix systems, The solution of the sparse matrix is carried out using SuperLU
#          Mumps -- ?
variable systemTypeStatic BandGeneral;
	# try UmfPack for large model
	set NodeTags [getNodeTags];
	set numNodes [llength $NodeTags];
		if {$numNodes>=500} {
			variable systemTypeStatic UmfPack;
		};
system $systemTypeStatic;
puts "YONG:System Type for Static Analysis is: $systemTypeStatic";
# _______________________________________________________________________________________________________
# _______________________________________________________________________________________________________

# TEST: convergence test to Convergence TEST (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/360.htm)
#   -- Accept the current state of the domain as being on the converged solution path 
#   -- determine if convergence has been achieved at the end of an iteration step
#          NormUnbalance -- Specifies a tolerance on the norm of the unbalanced load at the current iteration 
#          NormDispIncr -- Specifies a tolerance on the norm of the displacement increments at the current iteration 
#          EnergyIncr-- Specifies a tolerance on the inner product of the unbalanced load and displacement increments at the current iteration 
#          RelativeNormUnbalance --
#          RelativeNormDispIncr --
#          RelativeEnergyIncr --
variable TolStatic 5.0e-3;                  # Convergence Test: tolerance
variable maxNumIterStatic 2000;              # Convergence Test: maximum number of iterations that will be performed before "failure to converge" is returned
variable printFlagStatic 0;                # Convergence Test: flag used to print information on convergence (optional)        
											   # 1: print information on each step; 
											   # 2: print information on norms and number of iterations at end of successfull test
											   # 4: at each step it will print the norms and also the Delta_U and R(U) vectors.
											   # 5: if it fails to converge at end of $numIter it will print an error message BUT RETURN A SUCEESSFULL test 			       		   
variable testTypeStatic NormDispIncr ;	# Convergence-test type
 test $testTypeStatic $TolStatic $maxNumIterStatic $printFlagStatic;
puts "YONG:Convergency Test Type for Static Analysis is: $testTypeStatic with tolerance $TolStatic" ;
# for improved-convergence procedure:
	variable maxNumIterConvergeStatic 50;	
	variable printFlagConvergeStatic 2;
# _______________________________________________________________________________________________________
# _______________________________________________________________________________________________________

# Solution ALGORITHM: -- Iterate from the last time step to the current (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/682.htm)
#          Linear -- Uses the solution at the first iteration and continues 
#          Newton -- Uses the tangent at the current iteration to iterate to convergence
#                    -initial:optional flag to indicate to use initial stiffness iterations.
#                    -initialThenCurrent: optional flag to indicate to use initial stiffness on first step, then use current stiffness for subsequent steps.
#          ModifiedNewton -initial-- Uses the tangent at the first iteration to iterate to convergence,starting at a good initial guess U0. 
#          NewtonLineSearch <-type $typeSearch> <-tol $tol> <-maxIter $maxIter> <-minEta $minEta> <-maxEta $maxEta>-- 
#          KrylovNewton <-iterate $tangIter> <-increment $tangIncr> <-maxDim $maxDim>-- 
#          BFGS -- 
#          Broyden <$count>-- for general unsymmetric systems which performs successive rank-one updates of the tangent at the first iteration of the current time step.
variable algorithmTypeStatic ModifiedNewton -initial;
algorithm $algorithmTypeStatic;        
puts "YONG:Algorithm Type for Static Analysis is: $algorithmTypeStatic";
# _______________________________________________________________________________________________________
# _______________________________________________________________________________________________________

# Static INTEGRATOR: -- determine the next time step for an analysis  (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/689.htm)
#          LoadControl -- Specifies the incremental load factor to be applied to the loads in the domain 
#          DisplacementControl -- Specifies the incremental displacement at a specified DOF in the domain 
#          Minimum Unbalanced Displacement Norm -- Specifies the incremental load factor such that the residual displacement norm in minimized 
#          Arc Length -- Specifies the incremental arc-length of the load-displacement path 
# Transient INTEGRATOR: -- determine the next time step for an analysis including inertial effects 
#          Newmark -- The two parameter time-stepping method developed by Newmark 
#          HHT $alpha <$gamma $beta> -- The three parameter Hilbert-Hughes-Taylor time-stepping method,allows for energy dissipation 
#          HHT $alphaM $alphaF <$gamma $beta> -- Generalized alf integration object,allows for high frequency energy dissipation
#          Central Difference -- Approximates velocity and acceleration by centered finite differences of displacement 
#          TRBDF2 -- composite scheme that alternates between the Trapezoidal scheme and a 3 point backward Euler, It does this in an attempt to conserve energy and momentum, something newmark does not always do. 
variable integrationTypeStatic LoadControl; 
# integrator $integrationTypeStatic  $IDctrlNode   $IDctrlDOF $Dincr
# integrator ArcLength 0.01 1.0

set NstepGravity 50;  		# apply gravity in 10 steps
set DGravity [expr 1./$NstepGravity]; 	# first load increment;
integrator LoadControl $DGravity;	# determine the next time step for an analysis
puts "YONG:integrator Type for Static Analysis is: $integrationTypeStatic";
# _______________________________________________________________________________________________________
# _______________________________________________________________________________________________________

# ANALYSIS  -- defines what type of analysis is to be performed (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/324.htm)
#          Static Analysis -- solves the KU=R problem, without the mass or damping matrices. 
#          Transient Analysis -- solves the time-dependent analysis. The time step in this type of analysis is constant. The time step in the output is also constant. 
#          variableTransient Analysis -- performs the same analysis type as the Transient Analysis object. The time step, however, is variable. This method is used when 
#                 there are convergence problems with the Transient Analysis object at a peak or when the time step is too small. The time step in the output is also variable.
set analysisTypeStatic Static;
analysis $analysisTypeStatic;
puts "YONG:Analysis Type for Static Analysis is: $analysisTypeStatic"; 

