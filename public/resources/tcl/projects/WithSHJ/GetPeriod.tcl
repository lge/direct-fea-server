proc GetPeriod {args} {
	###########################################################################
	# GetOmega
	# or
	# GetOmega -nEigen $nEigen -PrintScreen $PrintScreen					
	###########################################################################
	#  perform eigenvalue analysis to determine periods
	#	   Silvia Mazzoni, 2008 (mazzoni@berkeley_NO_SPAM_.edu)
	# arguments:
	#	nEigen 		# number of specific eigenvalue to output periods (default: 1) may be of the format 'FirstEigen-LastEigen'
	#	PrintScreen	# trigger whether to output to screen or not (default: off)
	# select			# select all the eigenvalues or only one (default: all)
	#
	global TunitTXT
	set PI [expr 2*asin(1.0)];				# define constants
	# define defaults:
	set nEigen 1
	set PrintScreen off
	# override defaults with anything specified in argument list:
	if {[info exist args]} {
		foreach {Option Value} $args {
			if {[string index $Option 0]=="-"} {;							# remove the hyphen
				set Option [string range $Option 1 end]
			}
			set $Option $Value
			if {[llength $Option]>1} {;											# if the argument is a list
				foreach {SubName SubValue} $Option {
					if {[string index $SubName 0]=="-"} {;				# remove the hyphen
						set SubName [string range $SubName 1 end]
					}
					set $SubName $SubValue
				}
			}
		}
	}
	set fmt1 "Mode = %.1i: Period = %.3f"
	if {[info exists TunitTXT]} {
		append fmt1 " ($TunitTXT)"
	}
	set EigenList [split $nEigen -]
	if {[llength $EigenList]> 1} {;				# ask for more than one period
		set iTperiod ""
		set FirstEigen [lindex $EigenList 0]
		set LastEigen [lindex $EigenList 1]
		if {[catch {set lambdaN [eigen $LastEigen]}]} {
			set LastEigen [expr $LastEigen - 1] 
			if {[catch {set lambdaN [eigen $LastEigen]}]} {
				puts stderr ":: ERROR :: too many eigenvalues are being requested"
				return -1
			}
		}
		for {set i $FirstEigen} {$i <= $LastEigen} {incr i 1} {;		# first through last eigenvalue
			set lambda [lindex $lambdaN [expr $i-1]];
			set Omega [expr pow($lambda,0.5)]
			set Tperiod [expr 2*$PI/$Omega];	   	# period (sec.) 
			lappend iTperiod $Tperiod
			if {$PrintScreen == "on" | $PrintScreen == "yes"} {puts [format $fmt1 $i $Tperiod]}
		}
		return $iTperiod
	} else {;												# ask for only one period
		set nEigen [lindex $EigenList 0]
		set i $nEigen;		# only one eigenvalue
		set lambdaN [eigen $nEigen];					# returns list for eigenvalues 1-nEigen
		set lambda [lindex $lambdaN [expr $i-1]];
		set Omega [expr pow($lambda,0.5)]
		set Tperiod [expr 2*$PI/$Omega];	   	# period (sec.) 
		if {$PrintScreen == "on" | $PrintScreen == "yes"} {puts [format $fmt1 $i $Tperiod]};
		return $Tperiod
	}
}