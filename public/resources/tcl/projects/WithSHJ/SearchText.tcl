
    proc SearchText { infile eleID} {
        # open the target file for reading
		set infile tryRefineCommand.tcl;
        set in [open $infile r]
		
        set eleID  1;
		
        # upto the end-of-file print # eof: Check for end of file condition on channel
        # each line to the console
        while { [eof $in] != 1 } {
            gets $in line
			puts "$line"
			
			set numWord [llength $line]
			set yesorno [string compare [lindex $line [expr 0]] "element"]
			if {$yesorno==0} {
				if {[lindex $line [expr 2]]== $eleID} {
					set eleType [lindex $line [expr 1]];
					set eleIntPts [lindex $line [expr 5]];
					set eleSecID [lindex $line [expr 6]];
					set eleGeoTran [lindex $line [expr 7]];
					lappend eleInfo $eleType $eleIntPts $eleSecID $eleGeoTran
				}
            }
		}
	close $in
	return $eleInfo

    }
