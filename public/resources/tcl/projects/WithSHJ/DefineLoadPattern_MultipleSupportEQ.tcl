proc DefineLoadPattern_MultipleSupportEQ { LoadTag iSupportNode iGMdirection iGMfilename args} {
	##############################################################
	#  DefineLoadPattern_MultipleSupportEQ $LoadTag $iSupportNode  $iGMdirection $iGMfilename 
	###############################################################
	# define load pattern for ground-motion analysis -- multiple-support EQ excitation, unidirectional & bidirectional
	# define nodal displacements for Multiple-support excitation
	#	   Silvia Mazzoni, 2008 (mazzoni@berkeley_NO_SPAM_.edu)
	##   LoadTag:	unique load tag
	##   iSupportNode		# support Nodes where displacement histories are imposed.
	##   iGMdirection 		# lateral dof for ground motion input
	##   iGMfilename 		# ground-motion filename for input -- support 
	#
 	set iDefaultValue "GMdirectory . GMfactor 1.0  FileType Time&Displacement NumberHeaderLines 0 LoadFactor 1.0"
	foreach {Name DefaultValue} $iDefaultValue {
		set $Name $DefaultValue
	}
	# override defaults with anything specified in argument list:
	foreach {Option Value} $args {
		if {[string index $Option 0]=="-"} {;							# remove the hyphen
			set Option [string range $Option 1 end]
		}
		set $Option $Value
		if {[llength $Value]>1} {;											# if the argument is a list
			foreach {SubName SubValue} $Value {
				if {[string index $SubName 0]=="-"} {;				# remove the hyphen
					set SubName [string range $SubName 1 end]
				}
				set $SubName $SubValue
			}
		}
	}
	# --------------------------------- make lists the same size
	if {[info exists iGMfactor]!=1} {
		set iGMfactor ""
		foreach SupportNode $iSupportNode {
			lappend iGMfactor $GMfactor
		}
	}
	set iListName "iGMdirection iGMfilename iGMfactor"
	foreach ListName $iListName {
		eval "set theList $$ListName"
		if {[llength $theList]==1} {
			set OneValue $theList
			set theList ""
			foreach SupportNode $iSupportNode {
				lappend theList $OneValue
			}
			set $ListName $theList
		} elseif {[llength $theList]!=[llength $iSupportNode]} {
			puts stderr "ERROR: $ListName and iSupportNode must have the same number of elements"
			return -1
		}
	}
	# ---------------------------------
	
	# create load pattern
	# the following commands are unique to the Multiple-Support Earthquake excitation
	# multiple-support excitation: displacement input at individual nodes
	
	pattern MultipleSupport $LoadTag  {
		foreach SupportNode $iSupportNode GMfilename $iGMfilename GMfactor $iGMfactor GMdirection $iGMdirection {
			set GMseriesTag [expr $LoadTag*200+$SupportNode*4+$GMdirection]
			set inFile $GMdirectory/$GMfilename

			if {$FileType == "Time&Displacement" } {					# time and input are in two columns of one file
				set DisplSeries "Series -file $inFile -factor [expr $LoadFactor*$GMfactor]";		# time series information
			} elseif {$FileType == "DisplacementOnly" } {	;			# only Displacement is specified
				if {[info exists dt]!=1} {
					puts stderr "ERROR: Need to specify dt when using a FileType DisplacementOnly "
					return -1
				};	# this variable is used below
				set GMfatt [expr [expr $LoadFactor*$GMfactor]];			# data in input file is in g Unifts -- Displacement TH
				set DisplSeries "Series -dt $dt -filePath $inFile -factor  $GMfatt";		# time series information
			} elseif {$FileType == "PEER" } {		;		# best to specify cm -- four rows of header lines where dt is specified in line 4
				set outFile $GMdirectory/Mod${GMfilename};			# set variable holding new filename
				ReadSMDFile $inFile $outFile dt;			# call procedure to convert the ground-motion file
				if {[info exists cm]!=1} {
					puts stderr "ERROR: need to define cm to scale PEER ground motions"
					return -1
				};	# this variable is used below
				set GMfatt [expr $cm*[expr $LoadFactor*$GMfactor]];			# data in input file is in g Unifts -- Displacement TH
				set DisplSeries "Series -dt $dt -filePath $outFile -factor  $GMfatt";		# time series information
			} elseif {$FileType == "PEERNGA" } {		;		# best to specify g -- four rows of header lines where dt is specified in line 4
				set outFile $GMdirectory/Mod${GMfilename};			# set variable holding new filename
				ReadPEERNGAFile $inFile $outFile dt;			# call procedure to convert the ground-motion file
				if {[info exists cm]!=1} {
					puts stderr "ERROR: need to define cm to scale PEER-NGA ground motions"
					return -1
				};	# this variable is used below
				set GMfatt [expr $cm*[expr $LoadFactor*$GMfactor]];			# data in input file is in g Unifts -- Displacement TH
				set DisplSeries "Series -dt $dt -filePath $outFile -factor  $GMfatt";		# time series information
			} else {
				puts stderr "ERROR: FileType $FileType is not recognized. Options: Time&Displacement, DisplacementOnly, PEER, PEERNGA"
				return -1
			}
			groundMotion $GMseriesTag Plain -disp  $DisplSeries  
		    imposedSupportMotion $SupportNode  $GMdirection $GMseriesTag
			}; # end foreach SupportNode......
	};	# end pattern
}
