#  ProjectName: California High Speed RailWay System.
#  Define the load information based on CHSR
#  Developed by Yong LI, at University of California, San Diego
#  2012. March. 8th, foxchameleon@gmail.com

# Based on Table 12-1 in Chapter 12 -Structures of CHSR Design Guidelines.
set W_ElectrPerTrack     [expr 100.0*$plf];
set W_OCS_PolesSupport   [expr 0]; # Need to ask the guys in industry.
set W_CablePerWalkway    [expr 1400.0*$plf];
set W_Ballast            [expr 140.0*$pcf];
set W_ParapetWallPerSide [expr 800.0*$plf];
set W_RailsPerTrack      [expr 200.0*$plf];# ? why this value? different from what Tom said 141 plf?
set W_NonBallastPerTrack [expr 2200.0*$plf];
set W_Soils              [expr 0];  # Need to check the geotechnical Reports
set W_SoundWall          [expr 65.0*$plf];
set W_SystCablePerTrack  [expr 200.0*$plf];

# To compute the additional dead load.
set additionalDeadLoad1    [expr 9.4*$klf]; # Obtained from the report: Seismic Analysis from Parsons Brinckerhoff.
set tempAdditionalDeadLoad [expr $W_ElectrPerTrack*$numTrack];
set tempAdditionalDeadLoad [expr $tempAdditionalDeadLoad + $W_OCS_PolesSupport];
set tempAdditionalDeadLoad [expr $tempAdditionalDeadLoad + $W_CablePerWalkway*2];
set tempAdditionalDeadLoad [expr $tempAdditionalDeadLoad + $W_ParapetWallPerSide*2];
set tempAdditionalDeadLoad [expr $tempAdditionalDeadLoad + $W_SoundWall*2];
set tempAdditionalDeadLoad [expr $tempAdditionalDeadLoad + $W_SystCablePerTrack*$numTrack];
set tempAdditionalDeadLoad [expr $tempAdditionalDeadLoad + $W_NonBallastPerTrack*$numTrack];
set additionalDeadLoad2    [expr $tempAdditionalDeadLoad]; # Which is almost the same as additionalDeadLoad1
set additionalDeadLoad     [expr $additionalDeadLoad2];

set concreteDensity      [expr 155*$pcf/$g]; # Should we devide by g?, NO! YES, pcf is force in this unit system...
#  BUG: [expr 1/2] = 0!
set ballastLineMass      [expr 1.0/$numTrack*$additionalDeadLoad/$g]; # For each track
set railLineMass         [expr 0.5*$W_RailsPerTrack/$g];
set bmr 0.5; # The percentage of ballast mass to girder.

set pierColumnD          [expr 10*$ft];
set pierLineMass         [expr $concreteDensity*$pi*$pierColumnD*$pierColumnD/4];
set pierHeadArea         [expr 16*$ft*16*$ft]; # Be careful here we use the same head for all piers.
set pierHeadLineMass     [expr $concreteDensity*$pierHeadArea];
set pileCapArea   		 [expr 30*$ft*30*$ft];
set pileCapLineMass		 [expr $pileCapArea*$concreteDensity];

set isolatorMass         [expr 0.0];

set A_deckEndDiaphragm $A_deckEndDiaphragm; # Just to show that it has been defined.
set A_deckMiddle $A_deckMiddle;
set deckEndDiaphragmLineMass             [expr $A_deckEndDiaphragm*$concreteDensity];
set deckEndTransitionLineMass            [expr 0.5*($A_deckMiddle+$A_deckEndDiaphragm)*$concreteDensity];
set deckMiddleLineMass                   [expr $A_deckMiddle*$concreteDensity];
set deckEndDiaphragmLineRotationalMassX  [expr $Ip_SC_deckEndDiaphragm*$concreteDensity];
set deckEndTransitionLineRotationalMassX [expr 0.5*($Ip_SC_deckMiddle+$Ip_SC_deckEndDiaphragm)*$concreteDensity];
set deckMiddleLineRotationalMassX        [expr $Ip_SC_deckMiddle*$concreteDensity]; 
# Question:  But how about the rotational mass around Y and Z axis......

# Wind Loads

set deckWindPressure    [expr 50.0*$psf];
set columnWindPressure  [expr 75.0*$psf];
set deckWindHeightExtra [expr 10.0*$ft]; # Needs to be updated. 

set trainWindPressure   [expr 0.30*$klf];
set trainWindLocTOR     [expr 8.0*$ft];
set trainHeight         [expr 16.4*$ft];
set reducedFactor        [expr max(($trainHeight - $deckWindHeightExtra)/$trainHeight,0.5)];


# LLV or LLRR loads data information
	set numCars 10.0;
	set motorTailLenth    [expr 17.2*$ft];
	set trailerTailLength [expr 12.3*$ft];
	set AxlesDistance     [expr 57.4*$ft];
	set motorCarLength    [expr $motorTailLenth + $AxlesDistance + $trailerTailLength];
	set trailerCarLength  [expr $trailerTailLength + $AxlesDistance + $trailerTailLength];
	set maximumAxleLoad   [expr 12.95*$tons*$g];
	set trainWeight       [expr 500*$tons*$g];
	set trainLength       [expr 2.0*$motorCarLength + ($numCars-2.0)*$trailerCarLength];
	set LLVLineDensity    [expr (4.0*$numCars*$maximumAxleLoad + $trainWeight)/$trainLength];
	set LLVLineMass       [expr $LLVLineDensity/$g];
	# For now use the impact factor for LLRR
	if {$typicalSpanLength <= [expr 40*$ft] } {
		set LLVimpactFactor   [expr 100.0*(2.16/(sqrt(0.305*$typicalSpanLength)-0.2)-0.27)];
	} elseif { $typicalSpanLength > [expr 40*$ft] && $typicalSpanLength < [expr 127*$ft]} {
		set LLVimpactFactor   [expr 225/sqrt($typicalSpanLength/$ft)/100];
	} else {
		set LLVimpactFactor   [expr 0.20];
	}

	# Cooper E-50 Loading LLRR:
	set LLRRLineDensity   [expr 6700.0*$plf];
	set LLRRLineMass      [expr $LLRRLineDensity/$g];

	if {$typicalSpanLength <= [expr 40*$ft] } {
		set LLRRimpactFactor   [expr 100.0*(2.16/(sqrt(0.305*$typicalSpanLength)-0.2)-0.27)];
	} elseif { $typicalSpanLength > [expr 40*$ft] && $typicalSpanLength < [expr 127*$ft]} {
		set LLRRimpactFactor   [expr 225/sqrt($typicalSpanLength/$ft)/100];
	} else {
		set LLRRimpactFactor   [expr 0.20];
	}

	# Traction and Braking Forces for maintenance and construction trains (LF_LLRR)
	set cooperRatioToE80  0.625;
	set lengthConsidered  [expr $numSpan*$typicalSpanLength]; #length in feet of portion of bridge under consideration.
	set LLRRTraction       [expr $cooperRatioToE80*(25*sqrt($lengthConsidered/$ft))*$kip];
	set LLRRBraking        [expr $cooperRatioToE80*(45+1.2*($lengthConsidered/$ft))*$kip];
	set LLRRLineTraction   [expr $LLRRTraction/$lengthConsidered];
	puts "The LLRRLineTraction is [expr $LLRRLineTraction/1000*$ft] kips/ft";
	set LLRRLineBraking    [expr $LLRRBraking/$lengthConsidered];
	puts "The LLRRLineBraking is [expr $LLRRLineBraking/1000*$ft] kips/ft";
	set LLRRTractionZ_ROT  [expr 3*$ft];
	set LLRRBrakeZ_ROT   [expr 8*$ft];

	# Traction and Braking Forces for high-speed trains (LF_LLV)
	# CAUTION: THIS DENSITY CAN ONLY BE APLLIED WITHIN A LIMITED LENGTH to REACH UPPER BOUND in TOTAL....
	set LLVLineTraction   [expr 2.26*$kip/$ft];
	set LLVLineBraking    [expr 1.37*$kip/$ft];
	puts "The LLVLineTraction is [expr $LLVLineTraction/1000*$ft] kips/ft";
	puts "The LLVLineBraking is [expr $LLVLineBraking/1000*$ft] kips/ft";
		# These values needs to be checked....
		set LLVTractionZ_ROT  [expr 3*$ft];
		set LLVBrakeZ_ROT   [expr 8*$ft];