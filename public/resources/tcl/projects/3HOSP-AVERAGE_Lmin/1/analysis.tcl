# Source file titled 'analysis.tcl'
# Performs the following upon authoring:
# 1) define recorders
# 2) run base excitation

# Define RECORDERS ----------------------------------------------------------------------------------------------------
#source recorders.tcl;

# DYNAMIC ground-motion analysis -------------------------------------------------------------
set g 386.4;
set factor [expr $g*$scale]; 


set accelSeries "Series -dt $dt -filePath $eq_record -factor $factor";	# USE 2500 pts
pattern UniformExcitation 2 1 -accel $accelSeries;		# defining definition and location of acceleration in direction 1 with IDtag 2
set w1 [expr sqrt([lindex $eigenvalues 0])]
set w2 [expr sqrt([lindex $eigenvalues 1])]
set am [expr $damping*2.0*$w1*$w2/($w1+$w2)]
set bkinit [expr $damping*2.0/($w1+$w2)]
set bk     0.0
set bklast 0.0
rayleigh  $am $bk $bkinit $bklast


set iter 200;         
set tol 1e-6;
constraints Transformation;      				# how it handles boundary conditions
numberer Plain;
system BandGeneral;					# how to store and solve the system of equations in the analysis
test NormDispIncr $tol $iter;				# determine if convergence has been achieved at the end of an iteration step
#algorithm KrylovNewton ;					# use Newton's solution algorithm: updates tangent stiffness at every iteration
algorithm NewtonLineSearch 0.5;
integrator Newmark 0.5 0.25 ;			# determine the next time step for an analysis
#integrator HHT 0.5;
analysis Transient;					# define type of analysis: time-dependent
set tstep 0.010;	
set Nanaly [expr {round($Npoints*$dt/$tstep)+5/$tstep}];
puts "$Nanaly"
##
set tFinal [expr $Nanaly * $tstep]
puts "$tFinal"
set tCurrent [getTime]
set ok 0
# Perform the transient analysis
while {$ok == 0 && $tCurrent < $tFinal} {
     
    set ok [analyze 1 [expr $tstep]]
     # if the analysis fails try initial tangent iteration





    if {$ok != 0} {
       puts "Tstep*0.05 step failed .. lets tstep*0.0125"
        set ok [analyze 1 [expr ($tstep*0.0125)]]
        if {$ok == 0} {puts "that worked .. back to original procedure"}
        test NormDispIncr $tol $iter

	  algorithm NewtonLineSearch 0.5
    }


    if {$ok != 0} {
       puts "Tstep*0.0125 step failed .. lets tstep*0.005"
        set ok [analyze 1 [expr ($tstep*0.005)]]
        if {$ok == 0} {puts "that worked .. back to original procedure"}
        test NormDispIncr $tol $iter

	  algorithm NewtonLineSearch 0.5
    }


    if {$ok != 0} {
       puts "Tstep*0.005 step failed .. lets tstep*0.00125"
        set ok [analyze 1 [expr ($tstep*0.00125)]]
        if {$ok == 0} {puts "that worked .. back to original procedure"}
        test NormDispIncr $tol $iter

	  algorithm NewtonLineSearch 0.5
    }


    if {$ok != 0} {
       puts "Tstep*0.00125 step failed .. lets tstep*0.0005"
        set ok [analyze 1 [expr ($tstep*0.0005)]]
        if {$ok == 0} {puts "that worked .. back to original procedure"}
        test NormDispIncr $tol $iter

	  algorithm NewtonLineSearch 0.5
    }

    if {$ok != 0} {
       puts "Tstep*0.0005 step failed .. lets tstep*0.000125"
        set ok [analyze 1 [expr ($tstep*0.000125)]]
        if {$ok == 0} {puts "that worked .. back to original procedure"}
        test NormDispIncr $tol $iter

	  algorithm NewtonLineSearch 0.5
    }


    if {$ok != 0} {
       puts "Tstep*0.000125 step failed .. lets tstep*0.00005"
        set ok [analyze 1 [expr ($tstep*0.00005)]]
        if {$ok == 0} {puts "that worked .. back to original procedure"}
        test NormDispIncr $tol $iter

	  algorithm NewtonLineSearch 0.5
    }

    if {$ok != 0} {
       puts "Tstep*0.00005 step failed .. lets tstep*0.0000125"
        set ok [analyze 1 [expr ($tstep*0.0000125)]]
        if {$ok == 0} {puts "that worked .. back to original procedure"}
        test NormDispIncr $tol $iter

	  algorithm NewtonLineSearch 0.5
    }

    if {$ok != 0} {
       puts "Tstep*0.00000125 step failed .. lets try t*0.000005"
         set ok [analyze 1 [expr ($tstep*0.000005)]]
        if {$ok == 0} {puts "that worked .. back to original procedure"}
        test NormDispIncr $tol $iter
	  algorithm NewtonLineSearch 0.5
    

        set ok [analyze 1 [expr $tstep]]
        if {$ok == 0} {puts "that worked .. back to original procedure"}
        test NormDispIncr $tol $iter
        algorithm NewtonLineSearch 0.5;
    }
    set tCurrent [getTime]
}
# Print a message to indicate if analysis succesfull or not
if {$ok == 0} {
   puts "Transient analysis completed SUCCESSFULLY";
} else {
   puts "Transient analysis completed FAILED";    
}

puts "Finished!"
