OPERATING_SYSTEM=LINUX
GRAPHICS=NONE
RELIABILITY=NO_RELIABILITY
ifndef DEBUG_MODE
DEBUG_MODE = true
endif

# Directories:

PROJ_DIR = /var/www/cgi-bin/lge/sketchit-server
EXTERN_DIR = $(PROJ_DIR)/ext
SRC_DIR = $(PROJ_DIR)/src
ifeq ($(DEBUG_MODE),true)
BUILD_DIR = $(PROJ_DIR)/debug
else
BUILD_DIR = $(PROJ_DIR)/release
endif

FEA_DIR = $(EXTERN_DIR)/opensees
FE = $(FEA_DIR)/SRC
SRCdir = $(FEA_DIR)/SRC

NUMERICAL_DIR = $(EXTERN_DIR)/numerical
BLASdir      = $(NUMERICAL_DIR)/BLAS
CBLASdir     = $(NUMERICAL_DIR)/CBLAS
LAPACKdir    = $(NUMERICAL_DIR)/LAPACK
ARPACKdir    = $(NUMERICAL_DIR)/ARPACK
UMFPACKdir   = $(NUMERICAL_DIR)/UMFPACK
METISdir     = $(NUMERICAL_DIR)/METIS
SUPERLUdir   = $(NUMERICAL_DIR)/SuperLU_3.0/SRC
AMDdir       = $(NUMERICAL_DIR)/AMD 


# Commands:

MAKE = make
CD = cd
ECHO = echo
RM = rm
RMFLAGS = -f
SHELL = /bin/sh
MV = mv
CPP = g++
CC = gcc
LINKER =  g++
AR = ar
FC =  g77
RANLIB = ranlib


# Flags:

MVFLAGS = -f
ARFLAGS		= cqls
ifeq ($(DEBUG_MODE),true)
DEBUG_FLAG = -D_G3DEBUG
endif
CPPFLAGS = -Wall -g3 -D_LINUX -D_UNIX -D_TCL84 -D_NOGRAPHICS $(DEBUG_FLAG) $(PROGRAMMING_FLAG)  -ffloat-store -D_HTTPS
CFLAGS = -g3 -D_LINUX -D_UNIX -D_TCL84 -D_NOGRAPHICS $(DEBUG_FLAG) $(PROGRAMMING_FLAG) 
FFLAGS = -Wall 
LINKFLAGS = -rdynamic -Wall


# Include directories:

include $(FE)/Makefile.incl
MACHINE_INCLUDES = -I$(UMFPACKdir) -I$(SUPERLUdir) 
TCL_INCLUDES = -I/usr/local/tcl/include
SRC_INCLUDES = -I/$(SRC_DIR)/commands -I/$(SRC_DIR)/tcl -I/$(SRC_DIR)/websockets \
                             -I/$(SRC_DIR)/geometry  -I/$(SRC_DIR)/mesh
INCLUDES =  $(MACHINE_INCLUDES) $(FE_INCLUDES) $(TCL_INCLUDES) $(SRC_INCLUDES)

# Libraries:

MACHINE_SPECIFIC_LIBS = -ltcl8.4 -lssl -lz
FE_LIBRARY = $(FEA_DIR)/libOpenSees.a
NDARRAY_LIBRARY     = $(NUMERICAL_DIR)/libndarray.a
MATMOD_LIBRARY      = $(NUMERICAL_DIR)/libmatmod.a
BJMISC_LIBRARY      = $(NUMERICAL_DIR)/libBJmisc.a
LAPACK_LIBRARY      = $(NUMERICAL_DIR)/libLapack.a
BLAS_LIBRARY        = $(NUMERICAL_DIR)/libBlas.a
SUPERLU_LIBRARY     = $(NUMERICAL_DIR)/libSuperLU.a
CBLAS_LIBRARY       = $(NUMERICAL_DIR)/libCBlas.a
ARPACK_LIBRARY      = $(NUMERICAL_DIR)/libArpack.a
UMFPACK_LIBRARY     = $(NUMERICAL_DIR)/libUmfpack.a
METIS_LIBRARY       = $(NUMERICAL_DIR)/libMetis.a
AMD_LIBRARY         = $(NUMERICAL_DIR)/libAMD.a

MACHINE_NUMERICAL_LIBS = $(ARPACK_LIBRARY) \
  $(SUPERLU_LIBRARY) \
  $(UMFPACK_LIBRARY) \
  $(LAPACK_LIBRARY)  \
  $(BLAS_LIBRARY) \
  $(CBLAS_LIBRARY) \
  $(GRAPHIC_LIBRARY)\
  $(RELIABILITY_LIBRARY)\
  $(AMD_LIBRARY) -lg2c

WS_LIBARY = $(SRC_DIR)/websockets/libwebsockets.a
WS_LIBS = $(SRC_DIR)/websockets/base64-decode.o \
 $(SRC_DIR)/websockets/client-handshake.o \
 $(SRC_DIR)/websockets/extension.o \
 $(SRC_DIR)/websockets/extension-deflate-stream.o \
 $(SRC_DIR)/websockets/extension-x-google-mux.o \
 $(SRC_DIR)/websockets/handshake.o \
 $(SRC_DIR)/websockets/ibwebsockets.o \
 $(SRC_DIR)/websockets/md5.o \
 $(SRC_DIR)/websockets/parsers.o \
 $(SRC_DIR)/websockets/sha-1.o
 
INTERPRETER_LIBS =  $(SRC_DIR)/tcl/tclMain.o $(SRC_DIR)/tcl/tclAppInit.o $(SRC_DIR)/tcl/newCommands.o
                                  #$(SRC_DIR)/commands/commands.o
#GEOMETRY_LIBS = 
#MESH_LIBS = 

WIPE_LIBS = $(WS_LIBARY) $(INTERPRETER_LIBS) \
                    $(GEOMETRY_LIBS) $(MESH_LIBS)

# Rules:
.SUFFIXES:	.C .c .f .f90 .cpp .o .cpp

.DEFAULT:
	@$(ECHO) "Unknown target $@, try:  make help"

.cpp.o:
	@$(ECHO) Making $@ from $<
	$(CPP) $(CPPFLAGS) $(INCLUDES) -c $< 

.C.o:
	@$(ECHO) Making $@ from $<
	$(CPP) $(CPPFLAGS) $(INCLUDES) -c $< 

.c.o:
	@$(ECHO) Making $@ from $<
	$(CC) $(CFLAGS) -c $< -o $@

.f.o:      
	@$(ECHO) Making $@ from $<
	$(FC) $(FFLAGS) -c $< -o $@

.f77.o:      
	@$(ECHO) Making $@ from $<
	$(FC) $(FFLAGS) -c $< -o $@

.f90.o:      
	@$(ECHO) Making $@ from $<
	$(FC90) $(FFLAGS) -c $< -o $@
