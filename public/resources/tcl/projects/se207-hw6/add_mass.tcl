set PCol [expr -$pratio*$Ag*$unfc]
set LumpedMass [expr $PCol/$g];
mass $first_left_node $LumpedMass $LumpedMass 0.0
mass $first_right_node $LumpedMass $LumpedMass 0.0
mass $second_left_node [expr $LumpedMass/2] [expr $LumpedMass/2] 0.0
mass $second_right_node [expr $LumpedMass/2] [expr $LumpedMass/2] 0.0