pattern Plain 1 Linear {
   load $first_left_node 0.0 -$PCol 0.0
   load $first_right_node 0.0 -$PCol 0.0
   load $second_left_node 0.0 [expr -$PCol/2] 0.0
   load $second_right_node 0.0 [expr -$PCol/2] 0.0
}
# Gravity-analysis parameters -- load-controlled static analysis
set Tol 1.0e-8;				
constraints Plain;     			
numberer Plain;				
system BandGeneral;			
test NormDispIncr $Tol 10 ; 		
algorithm Newton;			
set NstepGravity 10;  			# set variable which will be used to apply gravity in 10 steps
set DGravity [expr 1./$NstepGravity]; 	# load increment
integrator LoadControl $DGravity;	
analysis Static;			
analyze $NstepGravity;			# apply vertical loads in 10 steps
# ------------------------------------------------- maintain constant gravity loads and reset time to zero
loadConst -time 0.0