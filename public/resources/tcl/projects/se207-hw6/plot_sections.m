close all
dirs={'force_1_5_p005_nonlinear' 'force_1_5_p005_linelastic'};
ele_conf='force-based element';
lstr={'nonlinear','linear elastic'};
Lcol=4;
f1=figure();
f2=figure();

markers={'-r' '--k' '-.b'};
for i=[1]
    deform_A=load([dirs{i} '/sectionA_deform.out']);
    force_A=load([dirs{i} '/sectionA_force.out']);
    deform_B=load([dirs{i} '/sectionB_deform.out']);
    force_B=load([dirs{i} '/sectionB_force.out']);
    
    figure(f1);
    hold on;    
    plot(deform_A(11:end,2),force_A(11:end,2)/1e3,markers{i});
        
    figure(f2);
    hold on;
    plot(deform_B(11:end,2),force_B(11:end,2)/1e3,markers{i});
    
end

figure(f1);
title(sprintf(['Moment-curvature response at section A\n',ele_conf]));
xlabel('Curvature');
ylabel('Moment (kN*M)');
grid on;
legend(lstr,'Location','SouthEast');

figure(f2);
title(sprintf(['Moment-curvature response at section B\n',ele_conf]));
xlabel('Curvature (rad/m)');
ylabel('Moment (kN*M)');
grid on;
legend(lstr,'Location','SouthEast');






