/** 
 * @author: Li Ge, lge@ucsd.edu
 * Module Name: Fe_Model_Manager.js
 * Description: 
 * Configure Options: 
 * Properties: 
 * Methods: 
 * Events: 
*/

var ut = require('util-lge');
var net = require('net');
var Fe_Model = require("./Fe_Model.js");

var defaults = {
    "opensees_tcp_port" : 8124
};

function Fe_Model_Manager(cfg) {
    ut.apply(this, defaults);
    if (ut.checkRequired(cfg, ['io'])) {
        var self = this;
        this._models_map = {};
        this._tcp_server = net.createServer(function(c) {
            console.log('server connected');
            c.on('end', function() {
                console.log('server disconnected');
            });
            c.on('data', function(data) {
                if (typeof c.id === 'undefined') {
                    c.id = data.toString();
                } else {
                    
                    self._io.sockets.in(c.id).emit('ops-json-domain', data.toString()); 
                }
            });
        });

        this._tcp_server.listen(self['opensees_tcp_port'], function() {
            console.log('tcp server listen port ' + self['opensees_tcp_port']);
        });

        this.bindSocketIO(cfg.io);
    } else {
        console.error("Error: Initialize Fe_Model_Manager without io");
    }    
}


Fe_Model_Manager.prototype.bindSocketIO = function(io) {
    var self = this;
    this._io = io;
    io.sockets.on('connection', function (socket) {
        var get_fe_model = function(data) {
            var mid = data;
            if (ut.isUndefined(mid)) {
                mid = ut.uniqueId("fe_model_");
            }
            var model = self._models_map[mid];
            if (ut.isUndefined(self._models_map[mid])) {
                model = self.addModel(mid);
            }
            model.addClient(socket);
        };

        var interp_ops_cmd = function(str) {
            if(ut.isDefined(socket.model_id) && ut.isDefined(self._models_map[socket.model_id])) {
                self._models_map[socket.model_id]
                    .interpretOpenSeesCmd(str);
            }
        };
        
        var cud_api = function(operation) {
            if(ut.isDefined(socket.model_id) && ut.isDefined(self._models_map[socket.model_id])) {
                return self._models_map[socket.model_id].crud(operation);
            } else {
                return false;
            }
        };
        
        socket.on('fe-model-join', function(data) {
            var mid = data || "default";
            get_fe_model(mid);
        });
        socket.on('fe-model-create', get_fe_model);
        socket.on('fe-model-ops-interp', interp_ops_cmd);
        socket.on('fe-model-cud', interp_ops_cmd);
        
        // old version api
        socket.on('ops-create-interp', get_fe_model);
        socket.on('ops-switch-room', get_fe_model);
        socket.on('ops-stdin', interp_ops_cmd);
        socket.on('ops-stdin', function(str) {
            console.log('write to ops stdin:\n' + str);
        });
        socket.on('message', function(data) {
            console.log('message:', data);
        });
    });
};

Fe_Model_Manager.prototype.ajaxAPI = function(req, res) {
    var sid = req.query.socket_id;
    var sc = this._io.sockets.socket(sid);
    var interp = this.getFeInterp();
    if (ut.isDefined(interp)) {
        interp.stdin.write(formatCommand(req.query.commands));
        res.jsonp({
            sid : sid ,
            room : sc.ops_room_id
        });
    } else {
        res.jsonp(500, {
            sid : sid ,
            room : sc.ops_room_id,
            error : true
        });
    }
};


Fe_Model_Manager.prototype.addModel = function(id) {
    var model, mid, interp;
    var self = this;
    if (ut.isDefined(id)) {
        mid = id;
    } else {
        mid = "default";
    }
    if (!ut.isDefined(this._models_map[mid])) {
        model = new Fe_Model({
            "id" : mid
        });
        this._models_map[mid] = model;
        interp = model.getFeInterp();

        interp.stdout.on('data', function(data) {
            self._io.sockets.in(model.id).emit('ops-stdout', data.toString());
        });
        
        interp.stderr.on('data', function(data) {
            self._io.sockets.in(model.id).emit('ops-stderr', data.toString());
        });
        
        interp.on('exit', function(code, sig) {
            console.log('interp with pid ' + this.ops_interp.pid +
                        ' exits, code: ' + code + ' signal: ' + sig);
            var clients = self._io.sockets.clients(model.id);
            if (clients.length === 0 && model.id !== 'default') {
                delete self._models_map[model.id];
            }
            
            self._io.sockets.in(model.id).emit('ops-server-closed', {
                code : code,
                signal : sig
            });
        });
        
        return model;
    } else {
        return false;
    }
};

Fe_Model_Manager.prototype.newModel = function() {
    var id = ut.uniqueId("fe_model_");
    return this.addModel(id);
};

module.exports = Fe_Model_Manager;