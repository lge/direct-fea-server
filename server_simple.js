var express = require('express');
var ut = require('util-lge');

var app = express();
var http = require('http');
var sio;

app.configure(function () {
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.set('view options', {
        layout: false
    });
    app.use(express.favicon());
    app.use(express.logger('dev'));

    app.use(express.static(__dirname + '/public'));

    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
});

app.configure('development', function () {
    app.use(express.errorHandler());
});


var server = http.createServer(app);
sio = require('socket.io').listen(server);
server.listen(3000);

sio.sockets.on('connection', function(socket) {
    socket.on('test', function(data, callback) {
        debugger;
        var d = ut.keys(data);
        callback(null, d);
    });
});

console.log("Express server listening on port 3000");