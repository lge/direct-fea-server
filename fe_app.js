/** 
 * @author: Li Ge, lge@ucsd.edu
 * Module Name: fe_app.js
 * Description: 
 * Configure Options: 
 * Properties: 
 * Methods: 
 * Events: 
*/
// var repl = require("repl");

var express = require('express');
var routes = require('./routes');
var fe_models = require('./sockets/fe_models.js');
var http = require('http');
var path = require('path');

var ut =require('util-lge');

var app = express();

app.configure(function(){
    app.set('port', process.env.PORT || 8080);
    app.set('opensees_exe', process.env.OPENSEES || 'myopensees');
    app.set('opensees_tcp_port', process.env.OPENSEES_TCP_PORT || 8124);
    app.set('gmsh_exe', process.env.GMSH || 'gmsh');
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
    app.use(express.errorHandler());
});

var server = http.createServer(app);
var io = require('socket.io').listen(server);
server.listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});


// var OMC = require('./models/OpenSees/OpenSees_Model_Collection');
// var omc = new OMC({
//     io : io
// });
// omc.bindSocketIO(io);

fe_models.init(sio);



// var r = repl.start({
// prompt: ">> ",
// input: process.stdin,
// output: process.stdout
// });
// r.context.app = app;
// r.context.io = io;
// r.context.ut = ut;
