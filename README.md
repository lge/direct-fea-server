# direct-fea-server #

A Finite Element Analysis (FEA) server that provides an integrated and
collaborative finite element analysis environment. Included modules are:

*  CAD modeling.
*  Mesh generation.
*  Finite Element Analysis.
*  Fields data streaming.

# Setup #

## Server: ##
1.  Install node and npm, install all dependencies using npm.
2.  Install extended OpenSees program.
3.  Install Gmsh.
4.  Configure:
    *  direct-fea service port number: default is `8080`
    *  OpenSees executable path: defaults is
       `~/Develop/OpenSees/BUILD/debug/bin/OpenSees`
    *  Gmsh executable path: default is `~/Develop/gmsh/build/gmsh`
    *  OpenSees port number: default is `8124`

## Client: ##

The client side talks to the server by emitting the `socket.io`
events. The socket.io based API will be documented below. The client
will need a socket.io protocol implementation. Please refer to
[socket.io-spec](https://github.com/LearnBoost/socket.io-spec).  Here
is an example to use the socket.io api in web browser. `socket.io.js`
can be downloaded from
[socket.io-client](https://github.com/LearnBoost/socket.io-client).

`index.html`:

    <!DOCTYPE html>
    <!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <html>
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <title>direct-fea-client TEST</title>
        <script src="./socket.io.js"></script>
        <script>
            // Connect the socket to the direct-fea service port.
            var ws = io.connect('localhost', { port : 8080 });
            ws.on('connect', function(){
                // After connection, the socket.io API is available to use.
                ws.emit('fe_models:ls', function(err, data){
                    console.log('Available models:', data);
                });
            });
     
            // The socket can also subscribe to the server side events:
            ws.on('fe_elements/1:update', function(diff){
                // Every time the element with id=1 changes
                // The client will receive the differences from server.
                
            })
        </script>
      </head>
      <body>
      </body>
    </html>



# API #

## client side events: ##
The basic patterns are:  

`ws.emit('${namespace}:${action}', [args1, args2...], [callback]);`

The `namespace` is a identifier that locates the object where the
`action` applies.

The `action` can be CRUD(Create, Read, Update, Delete) operations, it
can also be other more advanced operations that provided by the API.

The `callback` is a handler that will be called after get the server
response.  In convention, server will reply the result in two
argument, first one is a error object, second one is the result
object. The `callback` should be able to handle the error. A typical
callback function will be:

    var callback = function(error, result) {
        // do something here:

    };

*  name-space `fe_models`: 
   *  `'fe_models:create', data, [callback]`  
      Create a new fe_model and switch to it.
      *  data : Object, required. It must have a required field `name`.
      *  result : Object. A object that contains model info.
    
   *  `'fe_models:read'|'fe_models:ls', [filter], callback`  
      Get a list of active models.
      *  filter : Boolean function(fe_model)

   *  `'fe_models:join', model_name, callback`  
      *  model_name : String
      *  result : 

*  name-space `fe_model`: 
   *  `'fe_model:info', [key], callback`  
      Get the current fe_model info.
      *  key : string, optional. If no key specified, the model info are
         returned.
   
   All the following will get a response of the whole model data.
   *  `'fe_model:fetch'|'fe_model:read', callback`  
   *  `'fe_model:import_opensees_string', str, callback`  
   *  `'fe_model:import_opensees_file', filename, callback`  
   *  `'fe_model:import_geo_string', str, callback`  
   *  `'fe_model:import_geo_file', filename, callback`  
   *  `'fe_model:import_msh_string', str, callback`  
   *  `'fe_model:import_msh_file', filename, callback`  
   *  `'fe_model:import_stl_string', str, callback`  
   *  `'fe_model:import_stl_file', filename, callback`
   
*  name-space `fe_nodes`: 
*  name-space `fe_elements`: 

*  opensees
   *  `'opensees:import' commands`  
      *  commands : String. A OpenSees command string.

   *  `'opensees:analyze' analysis_settings`  
      *  analysis_setting : Object/String. A OpenSees command.

   *  `'opensees:send' commands`  
      *  commands : String. A OpenSees command.

*  gmsh
   *  `'gmsh:mesh' options callback`  


## server side events ##

Clients can subscribe to the model updates by adding listeners to
corresponding server pushed events.

*  fe_models: 
   *  `'fe_models:create', model`  
      Emitts when a new fe_model are created.
      *  model : Object. An object contains model info.
    
   *  `'fe_models:read'/'fe_models:ls', [filter], callback`  
      *  filter : Boolean function(fe_model)
      Get a list of active models.

   *  'fe_models:join', model_name, callback
      *  model_name : String
      *  result : 


*  fe_model: 


*  fe_solver:
   *  'fe_solver:analyze' analysis_setting

*  fe_fields


*  gmsh
   *  'gmsh:mesh' options callback


## client side examples:
* List all active fe_models on the server:

      var models_list;
      ws.once('fe_model-list', function(data){
          models_list = data;
      });
      ws.emit('fe_model-list');

* Get the whole model data and save it to a local `model` object:

      var model;
      ws.on('fe_model-data', function(data){
          model = data;
      });
      ws.emit('fe_model-crud', '/', 'read');

## Ajax API (TODO maybe)
